# `marquee-vue2`

> A simple marquee component with ZERO dependencies for Vue 2.

## 说明

> 组件为基于[vue3-marquee](https://github.com/megasanjay/vue3-marquee)转换的`Vue2`版本，使用方法参考原插件。
