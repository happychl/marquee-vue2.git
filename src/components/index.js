import VueMarquee from './vue-marquee.vue';

VueMarquee.install = function (Vue, options) {
  const name = options?.name ?? 'VueMarquee';
  Vue.component(name, VueMarquee);
};

export default VueMarquee;

export { VueMarquee };
